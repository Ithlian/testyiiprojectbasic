<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Class SignupForm
 * @package app\models
 */

class SignupForm extends Model{
    public $username;
    public $password;

    private $_user = false;

    /**
     * @return array
     */
    public function rules() {
        return [
            [['username', 'password'], 'required', 'message' => 'Заполните поле'],
            [['username', 'password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
        ];
    }

    public function save(){
        $this->_user = new User();
        $this->_user->username = $this->username;
        $this->_user->password = $this->password;
        $this->_user->role = 'user';
        $this->_user->save();
    }
}
